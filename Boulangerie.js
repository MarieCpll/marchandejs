//une méthode ajoute( produit )
//une méthode retire( produit )
//une proprieté totalHT
//une proprieté totalTTC


//Classe produit nom et prix
function Produit(n,p)
{
	this.nom = n;
	this.prix = p;
}

function Panier(totalHT,totalTTC)
{
	//Paramètres
	this.produits = [];
	this.totalHT = 0;
	this.totalTTC = 0;
	this.TVA = 1.055;

	//Méthodes
	this.ajouter = function(p)
	{
		this.produits.push(p);

		this.totalHT = this.totalHT + p.prix; //ou écrit : this.totalHT += p.prix
		this.totalTTC = this.totalHT * this.TVA;


	}
   
}

	//Ca C'est le Programme
 	let panier = new Panier();
    panier.ajouter(baguette);
    panier.ajouter(croissant);


    // Ce sont des instances de Classes
	let baguette = new Produit( 'Baguette', 0.85); // prix HT
    let croissant = new Produit( 'Croissant', 0.80);

    

    console.log(panier.totalHT);
    console.log(panier.totalTTC);   
